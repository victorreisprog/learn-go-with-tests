package helloworld

import "testing"

func TestHello(t *testing.T) {
	t.Run("saying hello to people", func(t *testing.T) {
	
		got := Hello("Chris", "english")
		want := "Hello, Chris"
		
		assertCorrectMsg(t, got, want)
	})

	t.Run("say 'Hello, World' when empty string is given", func(t *testing.T) {
		got := Hello("", "")
		want := "Hello, World"
		
		assertCorrectMsg(t, got, want)
	})

	t.Run("in Spanish", func(t *testing.T) {
		got := Hello("Elodie", "Spanish")
		want := "Hola, Elodie"

		assertCorrectMsg(t, got, want)
	})
}


func assertCorrectMsg(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
			t.Errorf("got %q want %q", got, want)
	}
}
