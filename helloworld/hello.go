package helloworld

import "fmt"

const (

spanish = "Spanish"
french = "French"

englishHelloPrefix = "Hello, "
spanishHelloPrefix = "Hola, "
frenchHelloPrefix = "Bonjour, "

)

func Hello(name, language string) string {
	if name == "" {
		name = "World"
	}

	return greetingPrefix(language) + name
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case french:
		prefix = frenchHelloPrefix
	case spanish:
		prefix = spanishHelloPrefix
	default:                   
		prefix = englishHelloPrefix
	}

	return // vai retornar "" por default, se fosse para retornar int ia retornar 0
}

func main() {
	fmt.Println(Hello("Elodie", spanish))
}
